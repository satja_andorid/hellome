package buu.satja.hellome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val edtName = findViewById<EditText>(R.id.edtName)
        val btnHello = findViewById<Button>(R.id.btnHello)
        val txtHello = findViewById<TextView>(R.id.TxtHello)
        //btnHello.setOnClickListener(this)
 //       btnHello.setOnClickListener(object: View.OnClickListener{
 //           override fun onClick(view: View?) {
 //               Toast.makeText(this@MainActivity, "Click Me", Toast.LENGTH_LONG).show()
 //          }
 //       })
        btnHello.setOnClickListener {
            if(edtName.text.toString() == ""){
                txtHello.text = "Please Enter Your Name!!"
            }else{
                Toast.makeText(this@MainActivity, "Hello " + edtName.text.toString(), Toast.LENGTH_LONG).show()
                txtHello.text = "Hello " + edtName.text.toString()
            }

        }
    }

    override fun onClick(view: View?) {
        Toast.makeText(this, "Click Me", Toast.LENGTH_LONG).show()
    }
}